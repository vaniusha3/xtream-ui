## xTream-UI
**[1]**

apt-get update ; apt-get install libxslt1-dev libcurl3 libgeoip-dev python -y ; wget https://bitbucket.org/vaniusha3/xtream-ui/downloads/install.py ; sudo python install.py

---

**[2]**

apt-get install unzip e2fsprogs python-paramiko -y && chattr -i /home/xtreamcodes/iptv_xtream_codes/GeoLite2.mmdb && rm -rf /home/xtreamcodes/iptv_xtream_codes/admin && rm -rf /home/xtreamcodes/iptv_xtream_codes/pytools && wget "https://bitbucket.org/vaniusha3/xtream-ui/downloads/xc_22f_update_19_12_20.zip" -O /tmp/update.zip -o /dev/null && unzip /tmp/update.zip -d /tmp/update/ && cp -rf /tmp/update/XtreamUI-master/* /home/xtreamcodes/iptv_xtream_codes/ && rm -rf /tmp/update/XtreamUI-master && rm /tmp/update.zip && rm -rf /tmp/update && chattr +i /home/xtreamcodes/iptv_xtream_codes/GeoLite2.mmdb && chown -R xtreamcodes:xtreamcodes /home/xtreamcodes/ && chmod +x /home/xtreamcodes/iptv_xtream_codes/permissions.sh && /home/xtreamcodes/iptv_xtream_codes/permissions.sh && /home/xtreamcodes/iptv_xtream_codes/start_services.sh

---

**[3]**
*If you use the XtreamUI v22f MODS v0.1 in nginx.conf add before last*

server {
    
        listen 80;
        root /home/xtreamcodes/iptv_xtream_codes/isp/;
        location / {
            allow 127.0.0.1;
            deny all;
        }
        location ~ \.php$ {
            limit_req zone=one burst=8;
            try_files $uri =404;
            fastcgi_index index.php;
            fastcgi_pass php;
            include fastcgi_params;
            fastcgi_buffering on;
            fastcgi_buffers 96 32k;
            fastcgi_buffer_size 32k;
            fastcgi_max_temp_file_size 0;
            fastcgi_keep_conn on;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_param SCRIPT_NAME $fastcgi_script_name;
        }
    }


---

**[4]**

sudo echo "@reboot root /home/xtreamcodes/iptv_xtream_codes/start_services.sh" >> /etc/crontab

---

**[5]**

chmod +x /home/xtreamcodes/iptv_xtream_codes/nginx_rtmp/sbin/nginx_rtmp

chmod +x /home/xtreamcodes/iptv_xtream_codes/nginx/sbin/nginx

sudo /home/xtreamcodes/iptv_xtream_codes/start_services.sh

---

**[6]**

wget https://bitbucket.org/vaniusha3/xtream-ui/downloads/config.py && python config.py ENCRYPT

**[7]**

cd / && rm -f install2.sh && bash <(curl -Ss https://bitbucket.org/vaniusha3/xtream-ui/downloads/install2.sh)